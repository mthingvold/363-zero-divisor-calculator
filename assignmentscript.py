import zerodiv
import argparse

#A quick script to get the answer to question 5
for i in range (2,13):
    print("Zero divisors of "+str(i))
    zerodiv.zero_div(i)
