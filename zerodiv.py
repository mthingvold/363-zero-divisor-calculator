import argparse




def zero_div(n):
    for i in range(1, n):
        for j in range(1, n):
            if (i*j)%n == 0:
                print(i, j)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Caculate zero divisors of integers in modulo n space.')
    parser.add_argument('integer', metavar='N', type=int, help='the modulo of the zero divisor being calculated')
    args = parser.parse_args()
    n = args.integer
    zero_div(n)